// toán tử so sánh => true hoặc false

// = ~ gán
// == ~ so sánh

var ss1 = 2 == 2;
var ss2 = 2 > 2;
var ss3 = 2 >= 1; // true
var ss4 = 2 === "2";
var ss5 = 2 !== 1; //true
var ss6 = 3 !== 3;

// toán tử so sánh : && || ( và , hoặc )
// khi cần kết hợp nhiều phép so sánh lại với nhau

// so sánh và: chỉ true khi tất cả đều đúng
// var ss7 = 2 > 1 && 2 > 4;
var ss7 = true && false;
var ss8 = true && true && false;

//  so sánh hoặc: chỉ false khi tất cả đều sai

var ss9 = false || true || false || false;
console.log("🚀 - file: index.js:24 - ss9", ss9);

// ||
("Cháu không có tiền, ko có nhà, đẹp trai, cháu lười");
console.log(`
       
       
       
       
      `);
