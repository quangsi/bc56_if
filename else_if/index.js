// 3 trường hợp trở lên

var n1 = 2;

var n2 = 4;

if (n1 > n2) {
  console.log("n2 nhỏ hơn");
} else if (n1 < n2) {
  console.log("n2 lớn hơn");
} else {
  console.log("n2 bằng n1");
}
// 5th

function tinhTien() {
  console.log("yes");
  var soLuong = document.getElementById("so-luong").value * 1;
  var donGia = document.getElementById("don-gia").value * 1;
  var tongTien = 0;
  if (soLuong < 50) {
    tongTien = soLuong * donGia;
  } else if (50 <= soLuong && soLuong <= 100) {
    tongTien = 49 * donGia + (soLuong - 49) * 0.92 * donGia;
  } else {
    tongTien =
      49 * donGia + 51 * 0.92 * donGia + (soLuong - 100) * 0.88 * donGia;
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h1 class="mt-5 p-3 alert-warning">${tongTien.toLocaleString()}</h1>`;
}
// 193 20.000

function xepLoai() {
  var diemToan = document.getElementById("diem-toan").value * 1;
  var diemLy = document.getElementById("diem-ly").value * 1;
  var diemHoa = document.getElementById("diem-hoa").value * 1;
  console.log({ diemToan, diemHoa, diemLy });
  var dtb = (diemToan + diemLy + diemHoa) / 3;
  if (dtb >= 8.5) {
    console.log("Giỏi");
  } else if (6.5 <= dtb) {
    console.log("Khá");
  } else if (5 <= dtb) {
    console.log("TB");
  } else {
    console.log("Yếu");
  }
  console.log("dtb", dtb);
}

// validate
