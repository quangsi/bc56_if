/**
 *
 * if( điều kiện đúng)
 * {
 *         thực hiện hành động
 *
 * }
 */

if (2 > 10) {
  console.log("yes");
}

var isLogin = true;

if (isLogin) {
  console.log("đăng nhập thành công");
}

if (!isLogin) {
  console.log("đăng nhập thất bại");
}
