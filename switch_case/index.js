var n = 20;

switch (n) {
  case 2: {
    console.log("số 2");
    break;
  }
  case 3: {
    console.log("số 3");
    break;
  }
  case 4: {
    console.log("số 4");
    break;
  }
  default: {
    console.log("Giá trị default");
  }
}

// switch case: so sánh bằng, ko so sánh được trong khoảng
